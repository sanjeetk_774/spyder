package at.favre.app.planb.demo;

import android.app.Application;
import android.os.StrictMode;

import com.squareup.leakcanary.DisplayLeakService;
import com.squareup.leakcanary.LeakCanary;

import at.favre.lib.planb.PlanB;
import at.favre.lib.planb.full.CrashDetailActivity;
import at.favre.lib.planb.util.CrashUtil;

public class CrashApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        setupLeakCanary();
        PlanB.get().init(true, PlanB.newConfig(this)
                .applicationVariant(BuildConfig.BUILD_TYPE, BuildConfig.FLAVOR)
                .scm(BuildConfig.GIT_REV, BuildConfig.GIT_BRANCH)
                .ci(BuildConfig.BUILD_NUMBER, BuildConfig.BUILD_DATE).build());
        setPlanBCrashReport();
    }

    protected void setupLeakCanary() {
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        //enabledStrictMode();
        LeakCanary.refWatcher(this).listenerServiceClass(DisplayLeakService.class).maxStoredHeapDumps(100).buildAndInstall();
    }

    private static void enabledStrictMode() {
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder() //
                .detectAll() //
                .penaltyLog() //
                .penaltyDeath() //
                .build());
    }

    public void setPlanBSuppress() {
        PlanB.get().enableCrashHandler(this, PlanB.behaviourFactory().createSuppressCrashBehaviour());
    }

    public void setPlanBCrashReport() {
        PlanB.get().enableCrashHandler(this, PlanB.behaviourFactory().createStartActivityCrashBehaviour(CrashDetailActivity.newInstance(this)));
    }

    public void setPlanBRestart() {
        PlanB.get().enableCrashHandler(this, PlanB.behaviourFactory().createRestartForegroundActivityCrashBehaviour());
    }

    public void setPlanBDefault() {
        PlanB.get().enableCrashHandler(this, PlanB.behaviourFactory().createDefaultHandlerBehaviour());
    }

    public void disableCrashHandling() {
        PlanB.get().disableCrashHandler();
    }

    public void crash() {
        CrashUtil.crash();
    }
}
