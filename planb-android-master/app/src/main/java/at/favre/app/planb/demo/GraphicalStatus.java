package at.favre.app.planb.demo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.List;

import at.favre.app.planb.demo.customviews.ArcProgress;
import at.favre.lib.planb.PlanB;
import at.favre.lib.planb.data.CrashData;

/**
 * Created by philips on 12/1/17.
 */

public class GraphicalStatus extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crash_graphical_view);

        List<CrashData> crashDataList = PlanB.get().getCrashDataHandler().getAll();
        ArcProgress SigninLogPercen = (ArcProgress) findViewById(R.id.arc_exception);
        SigninLogPercen.setProgress(crashDataList.size());

    }
}